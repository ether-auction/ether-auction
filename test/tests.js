const { expect } = require("chai");
const {deployments, getNamedAccounts} = require('hardhat');
//const { deployContract, loadFixture } = require("ethereum-waffle");
//const { ethers } = require("hardhat");


const ONE_DAY = 86400;
const BID_TIME = 7;
const EXPIRATION_TIME = 3;

describe("Auction", () => {
    // async function fixture([wallet, other], provider) {
    //     const auction = await deployContract(wallet, Auction, [BID_TIME, owner.getAddress()
    //     ]);
    //     return {auction, wallet, other};
    // }

    let owner, deployer, bidder1, bidder2, bidder3;
    let Auction, auction;

    const setupTest = deployments.createFixture(async ({deployments}) => {
        await deployments.fixture();
        [owner, bidder1, bidder2, bidder3] = await ethers.getSigners();
        const AuctionContract = await ethers.getContractFactory("MyAuction");
        auction = await AuctionContract.deploy(7, owner.getAddress());
        await auction.deployed();
        return auction
    });

    before(async () => {

    });

    beforeEach(async () => {
        const auction = await setupTest();
        return auction
    });

    describe("Deposit", () => {

        it("Should allow deposit on the pot", async function() {
            const before = await ethers.provider.getBalance(auction.address);
            await expect(auction.deposit_pot({value: 1}));
            const after = await ethers.provider.getBalance(auction.address);
            expect(after.sub(before)).to.equal(1);
            expect(await auction.pot()).to.equal(1);
        });
    });

    describe("Bid", () => {
        it("Should refuse bids on unfunded auctions", async function() {
            await expect(auction.connect(bidder1).bid({value: 1})).to.be.revertedWith('Cannot bid on inactive auction');
        });

        it("Should allow bid on funded auctions", async function() {
            await auction.deposit_pot({value: 1000});
            const before = await ethers.provider.getBalance(auction.address);
            await expect(auction.connect(bidder1).bid({value: 100}))
                .to.emit(auction, 'BidEvent')
            const after = await ethers.provider.getBalance(auction.address);
            expect(after.sub(before)).to.equal(100);

            expect(await auction.highest_bid()).to.equal(100);
            expect(await auction.highest_bidder()).to.equal(bidder1.address);
        });

        it("Should start the auction after the first bid", async function() {
            expect(await auction.auction_start()).to.equal(0);
            await auction.deposit_pot({value: 1000});
            await expect(auction.connect(bidder1).bid({value: 100}))
                .to.emit(auction, "StartEvent");

            expect(await auction.auction_start()).to.not.equal(0);
        });


        it("Should refuse lower or equal bids", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await expect(auction.connect(bidder2).bid({value: 100})).to.be.revertedWith('Can\'t bid, make a higher bid');
        });

        it("Should change highest bidder", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            expect(await auction.highest_bid()).to.equal(101);
            expect(await auction.highest_bidder()).to.equal(bidder2.address);
        });

        it("Should allow losers to up their bids", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder1).bid({value: 2});
            expect(await auction.highest_bid()).to.equal(102);
            expect(await auction.highest_bidder()).to.equal(bidder1.address);
        });

        it("Should refuse withdrawals from winner and loser", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await expect(auction.connect(bidder1).withdraw())
                .to.be.reverted;
            await expect(auction.connect(bidder2).withdraw()).to.be.reverted;

        });

        it("Should allow withdrawals from > 2nd highest bidder", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            expect(await auction.callStatic.bids(bidder1.address)).to.equal(100);
            const before = await ethers.provider.getBalance(auction.address);
            await expect(auction.connect(bidder1).withdraw())
                .to.emit(auction, 'WithdrawalEvent')
                .withArgs(bidder1.address, 100);
            const after = await ethers.provider.getBalance(auction.address);
            expect(before.sub(after)).to.equal(100);
        });

        it("Should prevent claiming on active auction", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await expect(auction.connect(bidder1).claim_pot()).to.be.reverted;//With("Auction hasn't ended!");
        });

        it("Should allow claiming on completed auction", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            ethers.provider.send("evm_increaseTime", [BID_TIME * ONE_DAY]);
            const before = await ethers.provider.getBalance(auction.address);
            await expect(auction.connect(bidder1).claim_pot()).to.be.reverted; // edge case
            await expect(auction.connect(bidder1).claim_pot())
                .to.emit(auction, "CompletedEvent")
                .to.emit(auction, "ClaimedEvent");
            const after = await ethers.provider.getBalance(auction.address);
            expect(before.sub(after)).to.equal(1000);
        });

        it("Should allow owner to take earnings on completed auction", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            ethers.provider.send("evm_increaseTime", [BID_TIME * ONE_DAY]);
            const before = await ethers.provider.getBalance(auction.address);
            expect(before).to.equal(1303);
            expect(await auction.claim_earnings());
            const after = await ethers.provider.getBalance(auction.address);
            expect(before.sub(after)).to.equal(203);
        });

        it("Should refuse bids on completed auctions", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            ethers.provider.send("evm_increaseTime", [BID_TIME * ONE_DAY]);
            await expect(auction.connect(bidder1).bid({value: 100}))
                .to.be.revertedWith("Cannot bid on inactive auction");

        });

        it("Should allow destruction of expired auction", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            ethers.provider.send("evm_increaseTime", [(BID_TIME * ONE_DAY) + 1]);
            await auction.claim_earnings();
            await expect(auction.destroy()).to.be.reverted;
            ethers.provider.send("evm_increaseTime", [3 * ONE_DAY]);
            await auction.destroy();
        });

        it("Should prevent destruction of expired auction by non-owner", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            ethers.provider.send("evm_increaseTime", [(BID_TIME * ONE_DAY) + 1]);
            await auction.claim_earnings();
            ethers.provider.send("evm_increaseTime", [3 * ONE_DAY]);
            await expect(auction.connect(bidder1).destroy())
                .to.be.reverted;
        })

        it("Should prevent destruction of active auction", async function() {
            await auction.deposit_pot({value: 1000});
            await auction.connect(bidder1).bid({value: 100});
            await auction.connect(bidder2).bid({value: 101});
            await auction.connect(bidder3).bid({value: 102});
            ethers.provider.send("evm_increaseTime", [BID_TIME * ONE_DAY]);
            await expect(auction.destroy()).to.be.reverted;
        })
    })

});
