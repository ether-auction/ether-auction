/**
 * @type import('hardhat/config').HardhatUserConfig
 */


// require("@nomiclabs/hardhat-vyper");
require("@nomiclabs/hardhat-waffle");
require('hardhat-deploy');

module.exports = {
  solidity: {
    version: "0.7.3",
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },

  namedAccounts: {
    owner: {
      default: 0
    },
    deployer: {
      default: 1
    },
    bidder1: {
      default: 2
    },
    bidder2: {
      default: 3
    },
    bidder3: {
      default: 4
    }
  }

};

task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});