Ether Auction
==

The Ether Auction is a variation of the dollar auction. It's like a traditional auction, except that both the first and second highest bids go to the auction's beneficiary, making the second highest bidder the 'biggest loser'. 
 
Losing bidders, except the biggest loser, will be able to withdraw their funds from the contract. The winner will claim the prize pot, thus triggering another auction. To start, we will auction off 1 ETH, and if the game theory is correct, the proceeds from the auction should surpass the initial pot, allowing subsequent auctions with a higher starting pot. 

Contract specifications
===

The contract is deployed by the owner, who needs only to specify the auction run time (in days). Bids are not allowed until a deposit is made to the pot. This first bid is the highest, and starts the countdown. Subsequent bids must be higher, and the previous winning bidder becomes the loser. 

The loser has three options:
 * Bid additional funds to become the highest bidder, cumulatively, and take the top spot again.
 * Wait for a third bidder to knock the current winner to loser, allowing the former loser to withdraw their funds. 
 * Forfeit their losing bid when the auction ends. 
 
After the auction's end time has passed, the winner may call the `claim pot` function to receive their winnings, and the owner may call `claim proceeds` to receive the first and second bids. The first of these functions to be called will mark the auction complete, which then starts the expiration time.  

Other bidders will have until the expiration time to withdraw their bids before the contract can be destroyed and the remaining funds forfeit to the contract owner.  


  


