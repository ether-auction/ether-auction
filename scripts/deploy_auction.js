const { ethers } = require("hardhat");

async function main() {
    let owner, deployer, bidder1, bidder2, bidder3;
    [owner, bidder1, bidder2, bidder3 ] = await ethers.getSigners();
    const Auction = await ethers.getContractFactory("MyAuction");
    //const { owner } = await ethers.getSigners();
    console.log("Deploying Auction...");
    const auction = await Auction.deploy(7, "0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266");
    await auction.deployed();
    console.log("Auction deployed to ", auction.address);
    await auction.connect(owner).deposit_pot({value: 10000000000});
    await auction.connect(bidder1).bid({value: 100});

}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error);
        process.exit(1);
    });