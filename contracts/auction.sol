pragma solidity ^0.7.3;
import "hardhat/console.sol";


contract Auction {
	address payable internal auction_owner;
	uint256 public auction_start;
	uint256 public bid_time;
	uint256 public auction_end;
	uint256 public auction_expiration;
	uint256 public expiration_time;

	uint256 public pot;

	uint256 public highest_bid; 
	address payable public highest_bidder;
	address public loser_bidder;
	uint256 public loser_bid;

	enum auction_state {
		NEW,  		// contract created
		PENDING, 	// pot seeded
		STARTED, 	// first bid; auction in process
		COMPLETED,	// past end time
		CANCELLED	// not used
		}

	//address[] bidders;
	mapping(address => uint) public bids;

	auction_state public STATE;

	modifier a_new_auction() {
		require(STATE == auction_state.NEW, "Auction isn't new");
		_;
	}

	modifier an_ongoing_auction() {
		require (is_active() == true, "Cannot bid on inactive auction");
		_;
	}

	function is_active() public returns (bool) {
		bool active = false;
		if (is_completed() == false) {
			if ((STATE == auction_state.PENDING) || (STATE == auction_state.STARTED)) {
				active = true;
			}
		}
		return active;
	}

	modifier a_completed_auction() {
		if (STATE != auction_state.COMPLETED) {
			require (is_completed() == true, "Auction hasn't ended!"); // Call will change STATE if true
		}
		_;
	}

	function is_completed() public returns (bool) {
		bool completed = false;
		if (auction_end != 0){
			if (block.timestamp > auction_end) {
				auction_expiration = block.timestamp + expiration_time * 1 days;
				STATE = auction_state.COMPLETED;					// These state changes will revert if called via
				emit CompletedEvent("Auction ended!", highest_bid); // bid() => an_ongoing_auction()
				completed = true;
			}
		}
		return completed;
	}

	modifier an_expired_auction() {
		require (auction_expiration > 0, "Auction hasn't completed");
		require (auction_expiration < block.timestamp, "Auction hasn't expired");
		_;
	}

	modifier only_owner() {
		require (msg.sender == auction_owner);
		_;
	}

	function deposit_pot() virtual public payable returns (bool) {}
	function bid() virtual public payable returns (bool) {}
	function withdraw() virtual public returns (bool) {}
	function complete() virtual public returns (bool) {}
	function claim_earnings() virtual public returns (bool) {}
	function claim_pot() virtual public returns (bool) {}
	function cancel_auction() virtual external returns (bool) {}

	event BidEvent(
		address indexed highest_bidder, 
		uint256 highest_bid
		); 
	
	event StartEvent (uint256 pot);
	event WithdrawalEvent (address withdrawer, uint256 amount);
	event CanceledEvent (string message, uint256 time);
	event CompletedEvent (string message, uint256 highest_bid);
	event ClaimedEvent (string message, address winner);
}

contract MyAuction is Auction {
	constructor (uint _biddingTime, address payable _owner) {
		auction_owner = _owner;
		bid_time = _biddingTime;
		expiration_time = 3;
		auction_expiration = 0;
		STATE = auction_state.NEW;
		pot = 0;
	}

	function deposit_pot() override public payable a_new_auction returns (bool) {
		pot = msg.value;
		STATE = auction_state.PENDING;
		return true;
	}

	function start_auction() internal returns (bool) {
		auction_start = block.timestamp;
		auction_end = auction_start + bid_time * 1 days;
		STATE = auction_state.STARTED;
		emit StartEvent(pot);
		return true;
	}

	function set_expiration() internal returns (bool) {
		auction_expiration = auction_start + expiration_time;
		emit StartEvent(pot);
		return true;
	}

	function bid() override public payable an_ongoing_auction returns (bool) {
		require (bids[msg.sender] + msg.value > highest_bid, "Can't bid, make a higher bid");
		if (STATE == auction_state.PENDING) {
			require (start_auction() == true, "Couldn't start auction");
		}
		loser_bidder = highest_bidder;
		loser_bid = highest_bid;
		highest_bidder = msg.sender;
		highest_bid = bids[msg.sender] + msg.value;
		bids[msg.sender] += msg.value;
		emit BidEvent(highest_bidder, highest_bid);
		return true;
	}

	function withdraw() override public returns (bool) {
		require (msg.sender != highest_bidder, "Can't withdraw, you're winning!");
		require (msg.sender != loser_bidder, "Can't withdraw, you're losing!");
		uint amount = bids[msg.sender];
		delete(bids[msg.sender]);
		(bool success, ) = msg.sender.call{value :amount}("");
		require(success, "Withdraw failed.");
		WithdrawalEvent(msg.sender, amount);
		return true;
	}

	function claim_earnings() override public a_completed_auction returns (bool) {
		uint256 amount = highest_bid + loser_bid;
		highest_bid = 0;
		loser_bid = 0;
		delete(bids[highest_bidder]);
		delete(bids[loser_bidder]);
		emit ClaimedEvent("Earnings claimed!", auction_owner);
		auction_owner.transfer(amount);
		return true;
	}

	function claim_pot() override public a_completed_auction returns (bool) {
		require(msg.sender == highest_bidder, "Can't claim pot, you didn't win!");
		bids[msg.sender] = 0;
		emit ClaimedEvent("Winnings claimed!", highest_bidder);
		msg.sender.transfer(pot);
		return true;
	}

	function destroy() public only_owner an_expired_auction {
		selfdestruct(auction_owner);
	}

}

	
